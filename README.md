1. Requirements :Apache Maven 3.3.3 , Java 8 
2. To run unit and integration test : mvn clean test
3. To run dev mode on port 8888: mvn jetty:run
   
   3.1 test with curl for happy path:
    
   curl -H "Content-Type: application/json" -X POST -d '{"roomSize":[5,5],"coords":[1,1],"instructions":"NE","patches": [[1, 0],[2,2],[2,3]] }'	 http://localhost:8888/hoover/  
 
   3.2 test with error: 
   
   curl -H "Content-Type: application/json" -X POST -d '{"roomSize":[5,5],"coords":[5,5],"instructions":"NNESEESWNWW","patches": [[1, 0],[2,2],[2,3]] }' http://localhost:8888/hoover/
    
4. To Package to war : mvn clean package 
 
 