package uk.co.tsong.hoover;


import org.jukito.JukitoModule;
import org.jukito.JukitoRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import uk.co.tsong.hoover.domain.CleanResult;
import uk.co.tsong.hoover.domain.Room;
import uk.co.tsong.hoover.exception.HooverMoveException;
import uk.co.tsong.hoover.juice.module.ApplicationModule;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(JukitoRunner.class)

public class HooveMoveServiceImpTest {

    public static class TestModule extends JukitoModule {

        @Override
        protected void configureTest() {
            install(new ApplicationModule());
        }
    }

    @Inject
    private HooverMoveService hooverMoveService;

    @Test
    public void testHooverMoveHappyPath()  {
        Room room = new Room();
        int[] hooverCoords = {1,1};
        int[] roomSzie = {5,5};
        room.setHooverCoords(hooverCoords);
        room.setRoomSize(roomSzie);
        int[] newCoords = hooverMoveService.moveDown(room).getHooverCoords();
        assertThat(newCoords[0], is(1));
        assertThat( newCoords[1],is(0));
    }

    @Test(expected=HooverMoveException.class)
    public void testMoveOutOfRoomWillGetException()  {
        Room room = new Room();
        int[] hooverCoords = {1,1};
        int[] roomSzie = {5,5};
        room.setHooverCoords(hooverCoords);
        room.setRoomSize(roomSzie);
        hooverMoveService.moveDown(room);
        int[] newCoords = hooverMoveService.moveDown(room).getHooverCoords();
        assertThat(newCoords[0], is(1));
        assertThat( newCoords[1],is(0));

    }

    @Test
    public void testValidateHooverLocationHappyPath()  {
        Room room = new Room();
        int[] hooverCoords = {1,1};
        int[] roomSzie = {5,5};
        assertThat(hooverMoveService.validateHooverLocation(roomSzie,hooverCoords), is(true));
    }

    @Test
    public void testValidateHooverLocationWithHooverOverRight()  {
        Room room = new Room();
        int[] hooverCoords = {6,1};
        int[] roomSzie = {5,5};
        assertThat(hooverMoveService.validateHooverLocation(roomSzie,hooverCoords), is(false));
    }

    @Test
    public void testValidateHooverLocationWithHooverOverTop()  {
        Room room = new Room();
        int[] hooverCoords = {1,6};
        int[] roomSzie = {5,5};
        assertThat(hooverMoveService.validateHooverLocation(roomSzie,hooverCoords), is(false));
    }

    @Test
    public void testValidateHooverLocationWithHooverOverLeft()  {
        Room room = new Room();
        int[] hooverCoords = {-1,6};
        int[] roomSzie = {5,5};
        assertThat(hooverMoveService.validateHooverLocation(roomSzie,hooverCoords), is(false));
    }

    @Test
    public void testValidateHooverLocationWithHooverOverBottom()  {
        Room room = new Room();
        int[] hooverCoords = {1,-1};
        int[] roomSzie = {5,5};
        assertThat(hooverMoveService.validateHooverLocation(roomSzie,hooverCoords), is(false));
    }

    @Test
    public void testCleanPatch()  {

        List<int[]> patches = new ArrayList<int[]>();
        int[] patch1={1,2};
        int[] hoooveMoveTo={1,2};
        int[] patch2={2,2};
        patches.add(patch1);
        patches.add(patch2);

        assertThat(hooverMoveService.cleanPatch(patches,patch1),is(true));
    }

    @Test
    public void testMoveAndCleanHappyPath()  {
        Room room = new Room();
        int[] roomSzie = {5,5};
        List<int[]> patches = new ArrayList<int[]>();
        int[] patch1={1,2};
        int[] patch2={2,2};
        int[] hooverCoords = {1,1};
        patches.add(patch1);
        patches.add(patch2);
        room.setHooverCoords(hooverCoords);
        room.setPatches(patches);
        room.setRoomSize(roomSzie);
        room.setInstructions("NE");
        List<int[]> cleanedPatches  = hooverMoveService.moveAndClean(room);
        assertTrue(Arrays.equals(patch1, cleanedPatches.get(0)));
        assertTrue(Arrays.equals(patch2, cleanedPatches.get(1)));
        assertThat(cleanedPatches.size(),is(2));
    }


    @Test
    public void testCleanupHappyPath() {
        Room room = new Room();
        int[] roomSzie = {5,5};
        List<int[]> patches = new ArrayList<int[]>();
        int[] patch1={1,2};
        int[] patch2={2,2};
        int[] hooverCoords = {1,1};
        patches.add(patch1);
        patches.add(patch2);
        room.setHooverCoords(hooverCoords);
        room.setPatches(patches);
        room.setRoomSize(roomSzie);
        room.setInstructions("NE");
        CleanResult cleanResult  = hooverMoveService.cleanup(room);
        assertThat(cleanResult.getPatches(), is(2));
        assertTrue(Arrays.equals(cleanResult.getCoords(),patch2 ));
    }
}
