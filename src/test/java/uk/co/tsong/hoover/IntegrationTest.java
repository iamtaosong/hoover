package uk.co.tsong.hoover;

import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jukito.JukitoModule;
import org.jukito.JukitoRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import uk.co.tsong.hoover.domain.CleanResult;
import uk.co.tsong.hoover.domain.ErrorResource;
import uk.co.tsong.hoover.juice.module.ApplicationModule;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


@RunWith(JukitoRunner.class)
public class IntegrationTest {

    private static final int TEST_PORT = findRandomOpenPort();
    private static final String TEST_BASE_URL = "http://localhost:" + TEST_PORT;
    private Server server;
    public static class TestModule extends JukitoModule {
        @Override
        protected void configureTest() {
            install(new ApplicationModule());

        }
    }
    public static Integer findRandomOpenPort() {
        try  {
            ServerSocket socket = new ServerSocket(0);
            return socket.getLocalPort();
        } catch (IOException e) {
            throw new IllegalStateException("Failed to find open port", e);
        }
    }

    @Before
    public void setup() throws Exception{

        server = new Server(TEST_PORT);
        WebAppContext context = new WebAppContext();
        context.setDescriptor("./src/main/webapp/WEB-INF/web.xml");
        context.setResourceBase("./src/main/webapp");
        context.setContextPath("/");
        context.setParentLoaderPriority(true);
        server.setHandler(context);
        server.start();
    }

    @After
    public void tearDownData() throws Exception{
        server.stop();
    }


    @Test
    public void testHappyPath() throws Exception{
        String json = "{\"roomSize\":[5,5],\"coords\":[1,1],\"instructions\":\"NE\",\"patches\": [[1, 0],[2,2],[2,3]] }";
        CleanResult cleanResult;
        int[] hooverCoords = {2,2};
        ObjectMapper objectMapper = new ObjectMapper();
        cleanResult = objectMapper.readValue(getJsonResult(json),CleanResult.class);
        assertThat(cleanResult.getPatches(),is(1));
        assertTrue(Arrays.equals(cleanResult.getCoords(), hooverCoords));
    }

    @Test
    public void testErrorOutput() throws Exception{
        String json = "{\"roomSize\":[5,5],\"coords\":[5,5],\"instructions\":\"NE\",\"patches\": [[1, 0],[2,2],[2,3]] }";
        ErrorResource resource;
        int[] hooverCoords = {2,2};
        ObjectMapper objectMapper = new ObjectMapper();
        resource = objectMapper.readValue(getJsonResult(json),ErrorResource.class);
        assertThat(  resource.getMessage(),is("Move out of top boundry"));


    }

    public String getJsonResult(String json) throws Exception {

        ClientRequest request = new ClientRequest(TEST_BASE_URL+"/hoover/");
        request.accept("application/json");
        request.body("application/json", json);
        ClientResponse<String> response = request.post(String.class);
        BufferedReader br = new BufferedReader(new InputStreamReader(
                new ByteArrayInputStream(response.getEntity().getBytes())));
        return br.readLine();

    }
}
