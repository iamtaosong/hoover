package uk.co.tsong.hoover;

import org.junit.Test;
import uk.co.tsong.hoover.util.Util;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class UtilTest {

    @Test
    public void testPatch()  {

        List<int[]> patches = new ArrayList<int[]>();
        int[] patch1={1,2};
        int[] patch2={2,2};
        patches.add(patch1);
        patches.add(patch2);
        int[] hoooveMoveTo={1,2};
        assertThat(Util.containsSubArray(patches,hoooveMoveTo),is(true));
        }

    }
