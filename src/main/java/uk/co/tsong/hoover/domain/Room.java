package uk.co.tsong.hoover.domain;


import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Room {

    private int[] roomSize = new int[2];

    private String instructions;

    List<int[]> patches = new ArrayList<int[]>();

    @JsonProperty("coords")
    private int[] hooverCoords = new int[2];

    public int[] getHooverCoords() {
        return hooverCoords;
    }
    public void setHooverCoords(int[] hooverCoords) {
        this.hooverCoords = hooverCoords;
    }

    public List<int[]> getPatches() {
        return patches;
    }

    public void setPatches(List<int[]> patches) {
        this.patches = patches;
    }

    public String getInstructions() {
        return instructions;
    }
    public void setInstructions(String directions) {
        this.instructions = directions;
    }

    public int[] getRoomSize() {
        return roomSize;
    }

    public void setRoomSize(int[] roomSize) {
        this.roomSize = roomSize;
    }


}

