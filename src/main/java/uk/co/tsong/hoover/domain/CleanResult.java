package uk.co.tsong.hoover.domain;


public class CleanResult {

    public CleanResult(){

    }

    private int[] coords = new int[2];

    private int patches;

    public int[] getCoords() {
        return coords;
    }

    public void setCoords(int[] coords) {
        this.coords = coords;
    }

    private int[] roomSize = new int[2];

    public int getPatches() {
        return patches;
    }

    public void setPatches(int patches) {
        this.patches = patches;
    }
}

