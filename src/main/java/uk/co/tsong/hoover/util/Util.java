package uk.co.tsong.hoover.util;

import java.util.Arrays;
import java.util.List;

/**
 * Created by tsong on 05/06/2016.
 */
public class Util {

    public static boolean containsSubArray(List<int[]> arrayList, int[] candidate) {
        for ( int[] arr : arrayList ) {
            if(Arrays.equals(arr, candidate)){
                return true;
            }
        }
        return false;
    }
}
