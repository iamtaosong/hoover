package uk.co.tsong.hoover.exception;

public class HooverMoveException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public HooverMoveException(String message) {
        super(message);

    }

    public HooverMoveException(String errorMessage, Throwable throwable) {
        super(errorMessage, throwable);
    }

    public HooverMoveException(Throwable throwable) {
        super(throwable);
    }

}
