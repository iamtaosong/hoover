package uk.co.tsong.hoover;

public enum HooverMoveDirection {

    NORTH("n"),
    SOUTH("s"),
    WEST("w"),
    EAST("e");


    private String message;

    HooverMoveDirection(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
