package uk.co.tsong.hoover.imp;

import com.google.inject.Singleton;
import uk.co.tsong.hoover.HooverMoveService;
import uk.co.tsong.hoover.domain.CleanResult;
import uk.co.tsong.hoover.domain.Room;
import uk.co.tsong.hoover.exception.HooverMoveException;
import uk.co.tsong.hoover.util.Util;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class HooverMoveServiceImp implements HooverMoveService {

    public CleanResult cleanup(Room room) {
        List<int[]> cleanedPatch=moveAndClean(room);
        CleanResult cleanResult = new CleanResult();
        cleanResult.setCoords(room.getHooverCoords());
        if(!cleanedPatch.isEmpty()){
            cleanResult.setPatches(cleanedPatch.size());
        }
        return cleanResult;
    }

    public boolean validateHooverLocation(final int[] roomSize, final int[] hooverLocation) {

        if( hooverLocation[0]<0){
            return false;
        }
        else if(roomSize[0]<hooverLocation[0]){
            return false;
        }
        else if( hooverLocation[1]<0){
            return false;
        }
        else if(roomSize[1]<hooverLocation[1]){
            return false;
        }
        return true;
    }

    public boolean cleanPatch(final List<int[]> patches, final int[] hooverLocation) {
        return  Util.containsSubArray(patches,hooverLocation);
    }

    public  List<int[]>  moveAndClean (  Room room) {

        List<int[]> cleanedPatches = new ArrayList<int[]>();

        for (char inst : room.getInstructions().toUpperCase().toCharArray()) {
            switch (inst) {
                case 'N':
                    moveUp(room); break;
                case 'S':
                    moveDown(room); break;
                case 'W':
                    moveLeft(room); break;
                case 'E':
                    moveRight(room);break;
                default:
                    break;
            }
            boolean isPatchCleaned = cleanPatch(room.getPatches(),room.getHooverCoords());
            if(isPatchCleaned){
                int[] coord = {room.getHooverCoords()[0], room.getHooverCoords()[1]};
                cleanedPatches.add(coord);
            }
        }
        return cleanedPatches;
    }

    public Room moveRight (Room room) throws HooverMoveException{

        int[] coords = { room.getHooverCoords()[0]+1,  room.getHooverCoords()[1]};
        if(validateHooverLocation(room.getRoomSize() , coords )){
            room.getHooverCoords()[0]++;
            return room;
        }
        else{
            throw new HooverMoveException("Move out of right boundry");
        }
    }

    public Room moveLeft (Room room) throws HooverMoveException  {

        int[] coords = { room.getHooverCoords()[0]-1,  room.getHooverCoords()[1]};
        if(validateHooverLocation(room.getRoomSize() , coords )){
            room.getHooverCoords()[0]--;
            return room;
        }
        else{
            throw new HooverMoveException("Move out of left boundry");
        }
    }

    public Room moveUp (Room room) throws HooverMoveException  {
        int[] coords = { room.getHooverCoords()[0],  room.getHooverCoords()[1]+1};
        if(validateHooverLocation(room.getRoomSize() , coords )){
            room.getHooverCoords()[1]++;

            return room;
        }
        else{
            throw new HooverMoveException("Move out of top boundry");
        }
    }

    public Room moveDown (Room room) throws HooverMoveException  {
        int[] coords = { room.getHooverCoords()[0],  room.getHooverCoords()[1]-1};
        if(validateHooverLocation(room.getRoomSize() , coords )){
            room.getHooverCoords()[1]--;

            return room;
        }
        else{
            throw new HooverMoveException("Move out of bottom boundry");
        }
    }
}

