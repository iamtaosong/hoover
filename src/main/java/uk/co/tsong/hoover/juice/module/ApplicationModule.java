package uk.co.tsong.hoover.juice.module;

import com.google.inject.AbstractModule;

public  class ApplicationModule extends AbstractModule {

    @Override
    protected void configure() {

        install(new HooveModule());

    }

}



