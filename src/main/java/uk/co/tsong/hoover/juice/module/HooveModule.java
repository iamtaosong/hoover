package uk.co.tsong.hoover.juice.module;

import com.google.inject.Binder;
import com.google.inject.Module;
import uk.co.tsong.hoover.HooveResource;
import uk.co.tsong.hoover.HooverMoveService;
import uk.co.tsong.hoover.endpoint.HooveResourceImp;
import uk.co.tsong.hoover.imp.HooverMoveServiceImp;

public class HooveModule implements Module {

    public void configure(Binder binder) {
        binder.bind(HooverMoveService.class).to(HooverMoveServiceImp.class);
        binder.bind(HooveResource.class).to(HooveResourceImp.class);
    }
}
