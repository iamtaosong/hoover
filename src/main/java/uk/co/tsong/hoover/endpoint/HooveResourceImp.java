package uk.co.tsong.hoover.endpoint;


import uk.co.tsong.hoover.HooveResource;
import uk.co.tsong.hoover.HooverMoveService;
import uk.co.tsong.hoover.domain.CleanResult;
import uk.co.tsong.hoover.domain.ErrorResource;
import uk.co.tsong.hoover.domain.Room;
import uk.co.tsong.hoover.exception.HooverMoveException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/hoover")

public class HooveResourceImp implements HooveResource {

    @Inject
    private HooverMoveService hooverMove;

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public  Response getResult(Room room) {

        try{
            CleanResult cleanResult = hooverMove.cleanup( room);
            return Response.status(Response.Status.OK).entity(cleanResult).build();
        }
        catch(HooverMoveException ex) {
            return Response.status(Response.Status.OK).entity(new ErrorResource(Response.Status.OK.toString(),ex.getMessage())).build();
        }
    }
}
