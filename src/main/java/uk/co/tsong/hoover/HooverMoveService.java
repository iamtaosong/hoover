package uk.co.tsong.hoover;

import uk.co.tsong.hoover.domain.CleanResult;
import uk.co.tsong.hoover.domain.Room;
import uk.co.tsong.hoover.exception.HooverMoveException;

import java.util.List;

public interface HooverMoveService {

    public CleanResult cleanup(Room room) throws HooverMoveException;

    public Room moveRight(Room room) throws HooverMoveException;

    public Room moveLeft(Room room) throws HooverMoveException;

    public Room moveUp(Room room) throws HooverMoveException;

    public Room moveDown(Room room) throws HooverMoveException;

    public boolean validateHooverLocation(final int[] roomSize, final int[] hooverLocation);

    public boolean cleanPatch(final List<int[]> patches, final int[] hooverLocation);

    public  List<int[]>  moveAndClean (  Room room) ;

}
